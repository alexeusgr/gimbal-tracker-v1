import React from "react";
import { graphql, Link } from "gatsby";
import Description from "../../components/Bounty/Description";
import BountyCard from "../../components/Bounty/BountyCard";
import AccessTokens from "../../components/AccessTokens";
import { Box, Text, Heading, Grid, GridItem, UnorderedList, ListItem } from "@chakra-ui/react";

const Bounties = ({ data }) => {
    const { edges } = data.allMarkdownRemark
    console.log(edges)
    return (
        <>
            <Box mx='auto' my='10' p='5' bg="white" w='80%'>
                <Description />
                <AccessTokens />
                <Grid mt='5' templateColumns='repeat(2, 1fr)' gap='4'>
                    {edges.map(edge => {
                        const { frontmatter } = edge.node
                        return (
                            <BountyCard info={frontmatter} />
                        )
                    })}
                </Grid>
            </Box>
        </>
    )
}

// Add a toggle for sort order

export const query = graphql`
    query BountyListQuery {
        allMarkdownRemark(
            sort: { order: ASC, fields: [frontmatter___slug]}
        ) {
            edges {
                node {
                    frontmatter {
                        title
                        slug
                        date
                        tags
                        ada
                        gimbals
                        scope
                    }
                }
            }
        }
    }
`

export default Bounties
