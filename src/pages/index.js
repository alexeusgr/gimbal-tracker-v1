import React, { useEffect, useState } from "react";
import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image"
import { useStoreState, useStoreActions } from "easy-peasy";
import Wallet from "../cardano/wallet";
import { fromAscii, fromHex } from "../utils/converter";
import { Flex, Center, Heading, Text, Box, List, ListItem, OrderedList, UnorderedList } from "@chakra-ui/react";
import { treasuryIssuerAddress } from "../cardano/treasury-contract";
import { getAddressKeyHash } from "../utils/factory";


const IndexPage = () => {
  const connected = useStoreState((state) => state.connection.connected);
  const [ walletFunds, setWalletFunds ] = useState(null);
  const [ walletUtxos, setWalletUtxos ] = useState(null)

  useEffect(async () => {
    if (connected) {
      await Wallet.enable();
      const amt = await Wallet.getBalance();
      setWalletFunds(amt);
      console.log(amt)
    }
  }, [])

  useEffect(async () => {
    if (connected) {
      await Wallet.enable();
      const amt = await Wallet.getUtxos();
      setWalletUtxos(amt);
      console.log(amt)
    }
  }, [])

  return (
    <>
      <title>ppbl demo</title>
      <Flex w='100%' mx='auto' direction='column' wrap='wrap' bg='gl-yellow'>
          <Box w={{ base: '90%', md: '70%', lg: '70%' }} mx='auto' my='5'>
            <Heading size='2xl' color='gl-blue' py='5' fontWeight='medium'>Gimbal Bounty Treasury and Escrow</Heading>
            <Text fontSize='sm' pb='5'>version 0.2.1 (May 2022)</Text>
            <Heading size='md' color='gl-blue' py='3'>This is a working example of a Dapp built on Cardano</Heading>
            <UnorderedList mb='3'>
              <ListItem ml='3'>End-to-end example: from Plutus code to user interface</ListItem>
              <ListItem ml='3'>Incomplete, unreliable, and not yet ready to scale</ListItem>
              <ListItem ml='3'>Built on a particular ReactJS stack with room for improvement</ListItem>
            </UnorderedList>
            <Heading size='md' color='gl-blue' py='3'>Why we're sharing it</Heading>
            <UnorderedList mb='3'>
              <ListItem ml='3'>To provide one example of a Cardano Dapp development stack</ListItem>
              <ListItem ml='3'>To highlight current development work that is happening across the Cardano community</ListItem>
              <ListItem ml='3'>To invite you to contribute to this project</ListItem>
            </UnorderedList>
            <Heading size='md' color='gl-blue' py='3'>To understand any Cardano Dapp, we should start by thinking about transactions</Heading>
            <UnorderedList mb='3'>
              <ListItem ml='3'>Plutus defines the rules for those transactions</ListItem>
              <ListItem ml='3'>Off-chain code - in this case, our front end - builds and submits those transactions to the blockchain</ListItem>
            </UnorderedList>
            <Heading size='md' color='gl-blue' py='3'>In this Dapp, there are 2 Contracts and 3 Transactions</Heading>
            <StaticImage src="../images/gbte-txs.png" alt="dapp transactions" width={800} />
            <Heading size='md' color='gl-blue' py='3'>In this mini-course, we will look at each Transaction:</Heading>
            <OrderedList mb='3'>
              <ListItem ml='3'>by viewing a demo of the Dapp.</ListItem>
              <ListItem ml='3'>by looking at the rules for each transaction in Plutus.</ListItem>
              <ListItem ml='3'>by investigating how the transactions are constructed in our front end.</ListItem>
            </OrderedList>
            <Heading size='md' color='gl-blue' py='3'>Finally, we'll take a step by step look at how to deploy a unique instance of this dapp.</Heading>
            <Heading size='md' color='gl-blue' py='3'>Along the way:</Heading>
            <UnorderedList mb='3'>
              <ListItem ml='3'>You might have questions. There are discussion boards in each Canvas module. Please use these to share your questions and new ideas.</ListItem>
              <ListItem ml='3'>We will highlight current development tasks.</ListItem>
              <ListItem ml='3'>Remember that this is just one example.</ListItem>
            </UnorderedList>
          </Box>
      </Flex>
    </>
  )
}

export default IndexPage;
