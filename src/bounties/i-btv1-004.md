---
slug: "i-btv1-004"
date: "2022-04-21"
title: "Create selection menu for multiple Access Tokens"
tags: ["Open"]
scope: "this-project"
ada: 3
gimbals: 10000
---

## Outcome: Implement a dropdown menu in the "Commit to Bounty" modal

## Files:
- `/src/templates/bountyPage.js`

## How to complete this bounty:

Submit a merge request to this project that adds a functioning dropdown menu to `bountyPage.js`. When the menu selection changes, the "Current Access Token" should change as well.
