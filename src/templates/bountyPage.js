import React, { useEffect, useState } from "react"
import { graphql, Link } from "gatsby";
import {
    Box, Text, Heading, Button, Center, Flex,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react";
import { useStoreState, useStoreActions } from "easy-peasy";
import useWallet from "../hooks/useWallet"
import { useUtxosFromAddress } from "../hooks/useUtxosFromAddress"
import { createBountyDatum, createTreasuryDatum } from "../utils/factory"
import { fromBech32, toStr, fromHex } from "../utils/converter";
import { serializeBountyDatum, deserializeBounty } from "../cardano/treasury-contract/datums";
import { treasuryIssuerAddress, treasuryContractAddress, bountyContractAddress, commitToBounty, accessPolicyID, bountyTokenUnit } from "../cardano/treasury-contract";

// Get markdown styles
import * as style from "./bountyDetails.module.css"

const Template = ({ data, pageContext }) => {
    // Get Bounty Data from Markdown FrontMatter
    const { markdownRemark } = data
    const { next, prev } = pageContext
    const title = markdownRemark.frontmatter.title
    const date = markdownRemark.frontmatter.date
    const slug = markdownRemark.frontmatter.slug
    const completed = markdownRemark.frontmatter.completed
    const tags = markdownRemark.frontmatter.tags
    const scope = markdownRemark.frontmatter.scope
    const ada = markdownRemark.frontmatter.ada
    const lovelace = ada * 1000000
    // -----------
    // Testnet:
    const gimbals = markdownRemark.frontmatter.gimbals
    // Mainnet
    // const fullGimbals = markdownRemark.frontmatter.gimbals
    // const gimbals = fullGimbals * 1000000
    // -----------
    const html = markdownRemark.html

    const { wallet } = useWallet(null)
    // Easy peasy stores connected Wallet Address and Wallet Utxos
    const connected = useStoreState((state) => state.connection.connected);
    const walletUtxos = useStoreState((state) => state.ownedUtxos.utxos)
    const setWalletUtxos = useStoreActions((actions) => actions.ownedUtxos.add)
    const accessTokens = useStoreState((state) => state.accessTokens.tokenNames);
    const [currentAccessToken, setCurrentAccessToken] = useState("")
    const [successfulTxHash, setSuccessfulTxHash] = useState(null)


    // Use this hook to query the Treasury Contract address
    const {
        utxos: treasuryUtxos,
        getUtxos,
        loading: utxosLoading,
        error,
    } = useUtxosFromAddress();

    // Initialize Modals
    const { isOpen: isCommitModalOpen, onOpen: onCommitModalOpen, onClose: onCommitModalClose } = useDisclosure()
    const { isOpen: isSuccessOpen, onOpen: onSuccessOpen, onClose: onSuccessClose } = useDisclosure()
    const { isOpen: isErrorOpen, onOpen: onErrorOpen, onClose: onErrorClose } = useDisclosure()

    // Use useUtxosFromAddress hook to grab the UTXO from Treasury Contract
    // + setWalletUtxos
    useEffect(() => {
        async function fetchData() {
            if (connected && wallet) {
                await getUtxos({
                    variables: {
                        addr: treasuryContractAddress,
                    },
                });
                const myUtxos = await wallet.utxos;
                setWalletUtxos(myUtxos);
            }
        }
        fetchData()
    }, [connected, wallet, utxosLoading, getUtxos])

    useEffect(() => {
        if (accessTokens.length > 0) {
            setCurrentAccessToken(toStr(fromHex(accessTokens[0].substring(56))))
        }
    }, [accessTokens])


    // Extract some info from the Treasury UTXO
    const [lovelaceAtTreasury, setLovelaceAtTreasury] = useState("0")
    const [tokensAtTreasury, setTokensAtTreasury] = useState("0")
    const [txHashAtTreasury, setTxHashAtTreasury] = useState(null)
    const [txIxAtTreasury, setTxIxAtTreasury] = useState("0")
    useEffect(() => {
        if (treasuryUtxos?.utxos[0].tokens) {
            setLovelaceAtTreasury(treasuryUtxos?.utxos[0].value)
            setTokensAtTreasury(treasuryUtxos?.utxos[0].tokens[0]?.quantity)
            setTxHashAtTreasury(treasuryUtxos?.utxos[0].transaction.hash)
            setTxIxAtTreasury(treasuryUtxos?.utxos[0].index)
        }
    }, [treasuryUtxos])

    const handleBountyCommitment = async () => {
        try {
            console.log("BUILD A BOUNTY COMMITMENT")
            console.log("treasury utxo", txHashAtTreasury, txIxAtTreasury)
            // construct Datum. We need one for each contract: Treasury + Bounty
            // Treasury Datum does not really "do" anything yet:
            const tDatum = createTreasuryDatum("101", treasuryIssuerAddress)
            // Bounty Datum
            const bDatum = createBountyDatum(treasuryIssuerAddress, connected, lovelace, gimbals, 10000000000)
            // Construct a Treasury UTXO that can be serialized in commitToBounty()
            const tUtxo = {
                "tx_hash": txHashAtTreasury,
                "output_index": txIxAtTreasury,
                "amount": [
                    { "unit": "lovelace", "quantity": `${lovelaceAtTreasury}` },
                    { "unit": `${bountyTokenUnit}`, "quantity": `${tokensAtTreasury}` }
                ],
            }
            console.log("Treasury UTXO", tUtxo)
            console.log("Building Commitments with amounts:", lovelace, gimbals)

            const bountyCommitment = {
                contributorAddress: connected,
                utxosParam: walletUtxos,
                bountySlug: slug,
                accessTokenName: currentAccessToken,
                bLovelace: lovelace, // from frontmatter
                bGimbals: gimbals, // from frontmater
                tUtxo,
                tLovelaceIn: lovelaceAtTreasury,
                tGimbalsIn: tokensAtTreasury
            }
            console.log("Here is your bountyCommitment:", bountyCommitment)

            // --- Not Essential - Playing with Datum: ---
            // serialize
            const serDatum = serializeBountyDatum(bDatum)
            console.log("Just to show that this works, here is your serialized Datum", serDatum)
            // deserialize
            console.log("And here is your deserialized Datum", deserializeBounty(serDatum))

            // see treasury-contract/index.js:
            const txHash = await commitToBounty(bDatum, tDatum, bountyCommitment)
            if (txHash.error) throw "error creating commitment TX"
            else {
                setSuccessfulTxHash(txHash);
                onCommitModalClose();
                onSuccessOpen();
            }
        } catch (error) {
            console.log("HANDLE BOUNTY ERROR", error)
            onCommitModalClose();
            onErrorOpen();
        }
    }

    return (
        <>
            <Box mx='auto' my='10' p='5' bg="white" w='50%'>
                <Heading size='lg'>
                    {title}
                </Heading>
                <Heading size='sm' py='1'>
                    Posted: {date} {completed ? `| Completed: ${completed}` : ""} | gimbals: {gimbals} | ada: {ada}
                </Heading>
                <Box bg='gl-yellow' my='3' p='2'>
                    <div dangerouslySetInnerHTML={{ __html: html }} className={style.mdStyle} />
                    <Flex direction='row' my='2'>
                        {tags?.map((tag) => <Box bg='gl-blue' color='white' fontWeight='bold' fontSize='xs' p='1' mx='1' textAlign='center' rounded='md'>{tag}</Box>)}
                        <Box bg='purple.500' color='white' fontSize='xs' fontWeight='bold' p='1' mx='1' textAlign='center' rounded='md'>{scope}</Box>
                    </Flex>
                </Box>


                {connected ? (
                    <Box bg="purple.200" p='5'>
                        <Button onClick={onCommitModalOpen}>Commit to Bounty {slug}</Button>
                    </Box>
                ) : (
                    <Box bg="red.200" p='5'>
                        To commit to a bounty, you must connect a wallet that holds an access token.
                    </Box>
                )}

                <Center pt='10'>
                    {prev &&
                        <Link to={`/bounties/${prev.frontmatter.slug}`}>
                            <Button mx='3' border='solid' borderColor='gl-blue'>
                                Previous
                            </Button>
                        </Link>
                    }
                    {next &&
                        <Link to={`/bounties/${next.frontmatter.slug}`}>
                            <Button mx='3' border='solid' borderColor='gl-blue'>
                                Next
                            </Button>
                        </Link>
                    }
                    <Link to="/bounties">
                        <Button mx='3' bg='gl-green' border='solid' borderColor='gl-green'>
                            View All
                        </Button>
                    </Link>
                </Center>
                <Box border='1px' mt='5' p='5'>
                    <Heading>How it Works</Heading>
                    <Box bg="purple.200" p='5'>
                        <Heading size='md'>Bounty Escrow Datum (and, because they have the same structure, the Treasury Redeemer) Require:</Heading>
                        <Text py='1'>1. A Bounty "Issuer's" Address and public key hash</Text>
                        <Text py='1'>2. If you hold an Access Token, you can be a "Contributor". We need the public key hash from your connected wallet.</Text>
                        <Text py='1'>3. Ada in Bounty (from frontmatter) = {ada}</Text>
                        <Text py='1'>4. Gimbals in Bounty (from frontmatter) = {gimbals}</Text>
                        <Text py='1'>5. Expiration time (this is not yet implemented)</Text>
                        <Text py='1'>6. Add the slug as metadata</Text>
                    </Box>
                    <Box bg="orange.200" p='5'>
                        <Heading>What's in the Treasury?</Heading>
                        <Text>this much lovelace: {lovelaceAtTreasury}</Text>
                        <Text>this much gimbals: {tokensAtTreasury}</Text>
                        <Text>These funds are in the TX: {txHashAtTreasury}#{txIxAtTreasury})</Text>
                        <Text>And i am the issuer: {treasuryIssuerAddress}</Text>
                    </Box>
                </Box>
                <Modal isOpen={isCommitModalOpen} onClose={onCommitModalClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Commit To Bounty</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Heading size='sm'>Bounty Id:</Heading>
                            <Text py='2'>
                                {slug} (to be included as transaction metadata)
                            </Text>
                            <Heading size='sm'>Current Access Token:</Heading>
                            <Text py='2'>
                                {currentAccessToken}
                            </Text>
                            <Heading size='sm'>Your Access Token:</Heading>
                            {accessTokens?.map(asset => {
                                const assetString = asset.substring(56)
                                const assetName = toStr(fromHex(assetString))
                                return (
                                    <Text py='2'>{assetName}</Text>
                                )
                            })}
                            <Heading size='sm'>Completion Deadline:</Heading>
                            <Text py='2'>
                                date
                            </Text>
                        </ModalBody>

                        <ModalFooter>
                            <Button colorScheme='blue' mr={3} onClick={onCommitModalClose}>
                                Close
                            </Button>
                            <Button colorScheme='purple' onClick={handleBountyCommitment}>Commit to this Bounty</Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
                <Modal isOpen={isSuccessOpen} onClose={onSuccessClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Commitment Tx Submitted</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Heading size='sm'>You successfully committed to this bounty</Heading>
                            <Text py='2' fontSize='xs'>Tx Hash: {successfulTxHash?.txHash}</Text>
                        </ModalBody>
                        <ModalFooter>
                            <Button colorScheme='blue' mr={3} onClick={onSuccessClose}>
                                Let's Go!
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
                <Modal isOpen={isErrorOpen} onClose={onErrorClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Commitment Tx Not Submitted</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Heading size='sm'>There was an Error with this commitment tx</Heading>
                            <Text>We are working on extending the error reporting for this Dapp (see bounty!). Please check the browser console to identify the error.</Text>

                        </ModalBody>

                        <ModalFooter>
                            <Button colorScheme='blue' mr={3} onClick={onErrorClose}>
                                ok
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
            </Box>
        </>
    )
}

export const query = graphql`
    query($pathSlug: String!) {
        markdownRemark(frontmatter: { slug: { eq: $pathSlug } }) {
            html
            frontmatter {
                title
                date
                completed
                tags
                scope
                ada
                gimbals
                slug
            }
        }
    }
`



export default Template